package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.dto.User;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Insert("INSERT INTO `user` " +
            "(`id`, `login`, `passwordHash`, `email`, `firstName`, `lastName`, `middleName`, `role`, `locked`) " +
            "VALUES (#{id}, #{login}, #{passwordHash}, #{email}, #{firstName}, #{lastName}, #{middleName}, #{role}, " +
            "#{locked})")
    void insert(@NotNull User user);

    @Update("UPDATE `user` " +
            "SET `login` = #{login}, `passwordHash` = #{passwordHash}, `email` = #{email}, `firstName` = #{firstName}, " +
            "`lastName` = #{lastName}, `middleName` = #{middleName}, `role` = #{role}, `locked` = #{locked} " +
            "WHERE `id` = #{id}")
    void update(@NotNull User user);

    @Delete("DELETE FROM `user` WHERE `id` = #{id}")
    void removeById(@NotNull @Param("id") String id);

    @Delete("DELETE FROM `user` WHERE `login` = #{login}")
    void removeByLogin(@NotNull @Param("login") String login);

    @NotNull
    @Select("SELECT * FROM `user`")
    List<User> findAll();

    @Nullable
    @Select("SELECT * FROM `user` WHERE `id` = #{id} LIMIT 1")
    User findById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM `user` WHERE `login` = #{login} LIMIT 1")
    User findByLogin(@NotNull @Param("login") String login);

    @Nullable
    @Select("SELECT * FROM `user` WHERE `email` = #{email} LIMIT 1")
    User findByEmail(@NotNull @Param("email") String email);

}
