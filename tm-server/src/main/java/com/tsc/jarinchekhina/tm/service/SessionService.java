package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.ISessionRepository;
import com.tsc.jarinchekhina.tm.api.service.IConnectionService;
import com.tsc.jarinchekhina.tm.api.service.IPropertyService;
import com.tsc.jarinchekhina.tm.api.service.ISessionService;
import com.tsc.jarinchekhina.tm.api.service.IUserService;
import com.tsc.jarinchekhina.tm.dto.Session;
import com.tsc.jarinchekhina.tm.dto.User;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyLoginException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyPasswordException;
import com.tsc.jarinchekhina.tm.exception.entity.UserNotFoundException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import com.tsc.jarinchekhina.tm.util.HashUtil;
import com.tsc.jarinchekhina.tm.util.SignatureUtil;
import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final IConnectionService connectionService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final IUserService userService;

    public SessionService(
            @NotNull final IConnectionService connectionService,
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService
    ) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final Session session) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            if (sessionRepository.findById(session.getId()) == null) sessionRepository.insert(session);
            sessionRepository.update(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void closeAll(@NotNull final List<Session> sessionList) {
        for (@Nullable final Session session : sessionList) {
            try {
                close(session);
            } catch (Exception e) {
                continue;
            }
        }
    }

    @Override
    public void close(@Nullable final Session session) {
        validate(session);
        removeById(session.getId());
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session) {
        if (session == null) throw new AccessDeniedException();
        if (DataUtil.isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (DataUtil.isEmpty(session.getUserId())) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final Session tempSession = session.clone();
        if (tempSession == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        if (DataUtil.isEmpty(signatureSource)) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sign(tempSession).getSignature();
        if (!signatureSource.equals(signatureTarget)) throw new AccessDeniedException();
        try (SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            @Nullable final Session sessionInRepo = sessionRepository.findById(session.getId());
            if (sessionInRepo == null) throw new AccessDeniedException();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final String secret = propertyService.getSessionSecret();
        @NotNull final Integer iteration = propertyService.getSessionIteration();
        @Nullable final String signature = SignatureUtil.sign(session, secret, iteration);
        session.setSignature(signature);
        return session;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findById(@NotNull String id) {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        try (SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findById(id);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> getListSession(@Nullable final Session session) {
        validate(session);
        try (SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findByUserId(session.getUserId());
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public User getUser(@Nullable Session session) {
        @NotNull final String userId = getUserId(session);
        @Nullable final User user = userService.findById(userId);
        return user;
    }

    @NotNull
    @Override
    public String getUserId(@Nullable final Session session) {
        validate(session);
        return session.getUserId();
    }

    @Override
    public boolean isValid(@Nullable final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session open(@Nullable final String login, @Nullable final String password) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (!checkDataAccess(login, password)) throw new AccessDeniedException();
        @Nullable User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        if (user.isLocked()) throw new AccessDeniedException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            @Nullable Session session = new Session();
            session.setUserId(user.getId());
            session.setTimestamp(System.currentTimeMillis());
            session = sign(session);
            if (session == null) throw new AccessDeniedException();
            if (sessionRepository.findById(session.getId()) == null) sessionRepository.insert(session);
            sessionRepository.update(session);
            sqlSession.commit();
            return session;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (DataUtil.isEmpty(login)) return false;
        if (DataUtil.isEmpty(password)) return false;
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @Nullable String passwordHash = HashUtil.salt(propertyService, password);
        if (DataUtil.isEmpty(passwordHash)) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String id) {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.removeById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        @Nullable final User user = userService.findById(userId);
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

}
