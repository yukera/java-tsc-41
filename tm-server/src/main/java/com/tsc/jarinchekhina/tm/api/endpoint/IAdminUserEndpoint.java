package com.tsc.jarinchekhina.tm.api.endpoint;

import com.tsc.jarinchekhina.tm.api.IEndpoint;
import com.tsc.jarinchekhina.tm.dto.Session;
import com.tsc.jarinchekhina.tm.dto.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IAdminUserEndpoint extends IEndpoint<User> {

    @NotNull
    User findByLogin(@Nullable Session session, @Nullable String login);

    @NotNull
    User lockByLogin(@Nullable Session session, @Nullable String login);

    void removeByLogin(@Nullable Session session, @Nullable String login);

    @NotNull
    User unlockByLogin(@Nullable Session session, @Nullable String login);

}
