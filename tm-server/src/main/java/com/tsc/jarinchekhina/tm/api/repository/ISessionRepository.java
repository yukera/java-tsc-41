package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.dto.Session;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    @Insert("INSERT INTO `session` " +
            "(`id`, `userId`, `signature`, `timestamp`) " +
            "VALUES (#{id}, #{userId}, #{signature}, #{timestamp})")
    void insert(@NotNull Session session);

    @Update("UPDATE `session` " +
            "SET `userId` = #{userId}, `signature` = #{signature}, `timestamp` = #{timestamp} " +
            "WHERE `id` = #{id}")
    void update(@NotNull Session session);

    @Delete("DELETE FROM `session`")
    void clear();

    @Delete("DELETE FROM `session` WHERE `id` = #{id}")
    void removeById(@NotNull @Param("id") String id);

    @NotNull
    @Select("SELECT * FROM `session`")
    List<Session> findAll();

    @NotNull
    @Select("SELECT * FROM `session` WHERE `userId` = #{userId}")
    List<Session> findByUserId(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM `session` WHERE `id` = #{id} LIMIT 1")
    Session findById(@NotNull @Param("id") String id);

}
