package com.tsc.jarinchekhina.tm.api;

import com.tsc.jarinchekhina.tm.dto.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {
}
