package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.api.IRepository;
import com.tsc.jarinchekhina.tm.dto.Task;
import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    @Insert("INSERT INTO `task` " +
            "(`id`, `name`, `description`, `dateStart`, `dateFinish`, `userId`, `status`, `projectId`) " +
            "VALUES (#{id}, #{name}, #{description}, #{dateStart}, #{dateFinish}, #{userId}, #{status}, " +
            "#{projectId})")
    void insert(@NotNull Task task);

    @Update("UPDATE `task` " +
            "SET `userId` = #{userId}, `name` = #{name}, `description` = #{description}, `dateStart` = #{dateStart}, " +
            "`dateFinish` = #{dateFinish}, `status` = #{status}, `projectId` = #{projectId} " +
            "WHERE `id` = #{id}")
    void update(@NotNull Task task);

    @Delete("DELETE FROM `task` WHERE `userId` = #{userId}")
    void clear(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM `task` WHERE `projectId` = #{projectId}")
    void removeAllByProjectId(@NotNull String projectId);

    @Delete("DELETE FROM `task` WHERE `userId` = #{userId} AND `id` = #{id}")
    void removeById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM `task` WHERE `userId` = #{userId} AND `name` = #{name}")
    void removeByName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

    @NotNull
    @Select("SELECT * FROM `task` WHERE `userId` = #{userId}")
    List<Task> findAll(@NotNull @Param("userId") String userId);

    @NotNull
    @Select("SELECT * FROM `task` WHERE `userId` = #{userId} AND `projectId` = #{projectId}")
    List<Task> findAllByProjectId(@NotNull @Param("userId") String userId, @NotNull @Param("projectId") String projectId);

    @Nullable
    @Select("SELECT * FROM `task` WHERE `userId` = #{userId} AND `id` = #{id} LIMIT 1")
    Task findById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM `task` WHERE `userId` = #{userId} LIMIT 1 OFFSET #{index}")
    Task findByIndex(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Nullable
    @Select("SELECT * FROM `task` WHERE `userId` = #{userId} AND `name` = #{name} LIMIT 1")
    Task findByName(@NotNull @Param("userId") String userId, @NotNull @Param("name") String name);

}
