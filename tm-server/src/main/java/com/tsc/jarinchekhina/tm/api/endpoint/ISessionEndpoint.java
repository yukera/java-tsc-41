package com.tsc.jarinchekhina.tm.api.endpoint;

import com.tsc.jarinchekhina.tm.api.IEndpoint;
import com.tsc.jarinchekhina.tm.dto.Session;
import com.tsc.jarinchekhina.tm.dto.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ISessionEndpoint extends IEndpoint<Session> {

    void closeSession(@Nullable Session session);

    @NotNull
    User getUser(@Nullable Session session);

    @Nullable
    Session openSession(@Nullable String login, @Nullable String password);

}
