package com.tsc.jarinchekhina.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;

@Setter
@Getter
@Entity
@NoArgsConstructor
public class Session extends AbstractEntity implements Cloneable {

    @Column
    @Nullable
    private Long timestamp;

    @Column
    @Nullable
    private String userId;

    @Column
    @Nullable
    private String signature;

    @Nullable
    @Override
    public Session clone() {
        try {
            return (Session) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
