package com.tsc.jarinchekhina.tm.command.user;

import com.tsc.jarinchekhina.tm.command.AbstractUserCommand;
import com.tsc.jarinchekhina.tm.endpoint.User;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserShowProfileCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-show";
    }

    @NotNull
    @Override
    public String description() {
        return "show user profile";
    }

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[USER PROFILE]");
        @NotNull final User user = serviceLocator.getSessionEndpoint().getUser(serviceLocator.getSession());
        print(user);
    }

}
